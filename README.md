<h1>MetaDev</h1>
<h3>Audemard Lucas - Lagrange Erwan - Ollé Benjamin - Solane Alexandre</h3>

Liste des différents outils à mettre en place, ainsi que le choix sur lequel on s'est porté :
<ul>
<li>OS utilisateur : Ubuntu 20.04</li>
<li>Gestion de cours en ligne : moodle</li>
<li>Solution cloud avec visionneuse d'image en ligne : nextcloud</li>
<li>Infrastructure pour le développement web : LAMP + PHPMyAdmin</li>
<li>Une forge et ticketing : gitlab</li>
<li>Une base unique d'authentification : OpenLDAP</li>
<li>Une VPN : OpenVPN</li>
<li>Un DNS : Bind</li>
</ul>
<h3>Des solutions libres et OpenSource</h3>

<h5>Ubuntu Host VM statement</h5>
<ul>
<li>Ubuntu 20.04 a été installé :<br/><strong>id : </strong><em>MetaDev</em><br/><strong>password : </strong><em>password</em></li>
<li>Docker.io and docker-compose ont été installés</li>
</ul>

<p>Nous avons fait le choix d'utiliser un fichier pour stocker un fichier .env pour y stocker les variables d'environnements (variables réutilisées plusieurs dans les scripts).<br/>De plus, les script proviennent des scripts fournis par les dépôts officiels de Docker Hub et les images associées aux différents services. Le choix de Traefik est de permettre l'accès aux différents services via un seuel support pour pas que ça soit trop pénible.</p>

<h5>Docker scripts : état du développement</h5>
<ul>
<li>Traefik script for routing : visualisation des frontend et backend actifs  &#x2705;</li>
<li>Portainer manager : gestion des networks, des images des containers et bien plus encore  &#x2705;</li>
<li>OpenLDAP : actif pour la base unique d'authentification  &#x2705;</li>
<li>Moodle : opérationnel et un peu long au démarrage  &#x2705;</li>
<li>NextCloud : opérationnel  &#x2705;</li>
<li>GitLab : opérationnel mais très long au démarrage  &#x2705;</li>
<li>OpenVPN : non fonctionnel  &#x274C;</li>
<li>Bind : non fonctionnel  &#x274C;</li>
</ul>

<h5>Suivi du déploiement</h5>
<ul>
<li>On crée un network frontend afin d'accéder à nos différents services via la commande : <strong>sudo docker network create web</strong> (cf fichier toml pour "web")</li>
<li>Lancement des services traefik et portainer pour la gestion UI de docker :
<ul>
<li><strong>sudo docker-compose -f metadev-prerequisites.yml up -d</strong> (#--remove-orphans) : à l'issue de cette phase les managers traefik et portainer sont accessibles aux adresses traefik.localhost et portainer.localhost (cf fichier YAML pour les adresses de traefik)</li> 
<li><strong>sudo docker-compose -f metadev-services.yml up -d</strong> : lancement des services pour la société Metadev </li>
</ul>
</li>
</ul>

<h5>Gestion de Docker portainer</h5>
<ul>
<li>Lors de la première connexion renseigner un mot de passe pour le compte admin : password</li>
<li>Sélectionner la gestion du stack local puis s'y connecter</li>
<li>Vous avez dès maintenant accès à toutes les informations de manière graphique</li>
</ul>

<h5>Gestion du LDAP</h5>
<ul>
<li>Étape 1 : Configuration de LDAP admin
<ul>
<li>Se rendre sur ldapadmin : openldap-admin.localhost</li>
<li>Se connecter avec les identifiants : <br/><strong>id : </strong><em>cn=admin,dc=metadev,dc=com</em><br/><strong>password : </strong><em>admin</em>
<li>Cliquer sur "+" puis sur "Create new entry here" : choisir "Generic Organisational Unit", saisir group puis create et commit</li>
<li>Cliquer sur l'entrée group, puis "Create a child entry" : choisir "Generic Posix Group" saisir users, puis create et commit</li>
<li>Cliquer sur l'entrée users dans group, puis "Create a child entry" : choisir " Generic : User Account ", saisir :
<ul>
<li>Firstname : John</li>
<li>Lastname : Doe</li>
<li>Common name : autocomplétion en cliquant</li>
<li>userid : jdoe autocomplétion en cliquant</li>
<li>password : password</li>
<li>GID number : users</li>
</ul>
Puis create et commit
</li>
</ul>
</li>
<li>Étape 2 : Configuration de LDAP Nextcloud
<ul>
<li>Accéder à nextcloud.localhost</li>
<li>Enregistrer un user admin sur NextCloud (id: admin; password: password), laisser NextCloud installé ses apps</li>
<li>Rendez-vous sur app depuis l'icône du compte, puis "Disabled apps" : Cliquer sur "Enable" en face de "LDAP user and group backend"</li>
<li>Accéder à Settings, puis dans la rubrique Administration, cliquer sur "LDAP / AD Integration", saisir :
<ul>
<li>Host : openldap et détecter le port automatique ou renseigner le port 389</li>
<li>user DN : cn=admin,dc=metadev,dc=com</li>
<li>password: admin</li>
<li>Cliquer sur Detect Base DN puis Test Base DN</li>
<li>Advanced --> Directory Settings --> User Display Name Field = uid</li>
<li>Users : choisir posixAccount</li>
<li>Groups : choisir posixAccount et users</li>
</ul>
Puis vous pouvez vous déconnecter et vous reconnecter à l'aide des identifiants de John Doe : jdoe / password
</li>
</ul>
</li>
<li>Étape 3 : Configuration de LDAP Moodle
<ul>
<li>Accéder à moodle.localhost lorsque le service est chargé (cf logs de moodle depuis portainer.localhost)</li>
<li>Se connecter à l'aide des identifiants : user / bitnami</li>
<li>Cliquer sur "Site Administration", puis "Authentification", puis "Manage Authentification"</li>
<li>Cliquer sur LDAP Server settings, puis saisir :
<ul>
<li>Host URL : openldap</li>
<li>Prevent password caching : YES</li>
<li>Distinguished name : cn=admin,dc=metadev,dc=com</li>
<li>Password : admin</li>
<li>User Type : posixAccount (rfc2307)</li>
<li>Contexts : dc=metadev,dc=com</li>
<li>Search subcontexts : YES</li>
<li>User attribute : uid</li>
<li>Member attribute : memberuid</li>
<li>Password format : MD5 hash</li>
<li>Data mapping Firstname : givenName</li>
<li>Data mapping Surname : sn</li>
<li>Data mapping Email address : mail</li>
<li>Data mapping Phone : telephoneNumber</li>
<li>Data mapping Address : postalAddress</li>
</ul>
Puis cliquer sur "save changes", passer LDAP SERVER en enable et up le service dans "Manage Authentification", vous pouvez alors vous déconnecter et vous reconnecter à l'aide des identifiants de John Doe : jdoe / password
</li>
</ul>
</li>
<li>Étape 4 : Configuration de LDAP GitLab
<ul>
<li>Accéder à gitlab.localhost lorsque le service est chargé (cf logs de gitlab depuis portainer.localhost), gitlab est lancé lorsque son état est à healthy dans portainer</li>
<li>Se connecter à l'aide des identifiants : jdoe / password</li>
<li>La configuration est renseignée dans le YAML au service gitlab via les variables d'environnement renseignées</li>
<li>Le LDAP ne fonctionne pas pour gitlab --> aucune idée (cf même config que sur  les documentations et plusieurs sujets résolus sur gitlab et stackoverflow</li>
<li>Par ailleurs à la première connexion renseigner "password" pour le mot de passe du compte root de gitlab. On peut alors s'y connecter par la voie classique --> On ne voit pas d'utilisateur LDAP dans le settings</li>
</ul>
</li>
</ul>

<h5>Gestion du serveur LAMP</h5>
<ul>
<li>Insérer un fichier index.php dans containers_volumes/lamp/www afin de vérifier le fonctionnement de apache et PHP à partir de apache.localhost</li>
<li>C'est dans ce répertoire que se trouveront les sites développés, vous pouvez par ailleurs faire des vhosts de manière classique</li>
<li>L'accès à PHPMyAdmin se fait depuis phpmyadmin.localhost à l'aide des identifiants : root / root-password (pour la gestion admin), ou apache-user / apache-password (pour la gestion propre à vos sites)</li>
</ul>
